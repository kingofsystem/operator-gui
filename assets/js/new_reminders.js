request = require('request');
remote = require('electron').remote;
pug = require('pug');
path = require('path');
const Config = require('electron-config');

const AppSettings = new Config();

// False when its first start of script
let new_reminders_view_loaded = false;
let intervalId = 0;

function polling() {
    getUnreadedReminders();
    intervalId = setTimeout(polling, 5000);
}

function closeView() {
    new_reminders_view_loaded = false;
    clearInterval(intervalId);
    delete intervalId;
}

// START POLLING
polling();

function getUnreadedReminders() {
    makeRequest('/new_reminders/', 'get', null, function (err, response, body) {
        let list = JSON.parse(body);
        for (let measure of list) {
            console.log(measure);
            if (measure['viewed'])
                continue;
            measure['pressure_and_pulse'] = JSON.parse(measure['pressure_and_pulse']);

            const card = reminderAlreadyAdded(measure);
            if (!card) {
                let compiledPug = pug.compileFile(path.join(__dirname, 'sections/reminder_card.pug'));

                let html = compiledPug({
                    card_title: measure['lastname'] + ' ' + measure['name'][0] + '. ' +
                        ((measure['surname'] !== '') ? measure['surname'][0] + '.' : ''),
                    measure_text: construct_card_measure_text(measure),
                    measure_date: construct_card_date(measure),
                    measure_id: measure['id'],
                    photo_url: measure['current_photo'],
                    status_good_click: 'reminder_status(this, false)',
                    status_bad_click: 'reminder_status(this, true)'
                });

                $('#main-block-content').prepend(html);

                if (new_reminders_view_loaded && AppSettings.get('show_notifications')) {
                    new Notification(measure['lastname'] + ' ' + measure['name'][0] + '. ' +
                        ((measure['surname'] !== '') ? measure['surname'][0] + '.' : ''), {
                        tag : "ache-mail",
                        body : "Новое измерение",
                        icon : fix_photo_url(measure['current_photo'])
                    });
                }
            } else {
                update_card(card, measure)
            }
        }
        new_reminders_view_loaded = true;
    })
}

function reminderAlreadyAdded(measure) {
    for (let reminder_card of $('.reminder')) {
        if (reminder_card.getElementsByClassName('measure_id')[0].innerText === measure['id'].toString())
            return reminder_card;
    }
    return false;
}

function construct_card_measure_text(measure) {
    return 'Алкоголь: ' + ((measure['promile'] === 0.0) ? 'в норме' : measure['promile'] + ' промиллей') + '. ' +
            'Давление: ' + measure['pressure_and_pulse'][0] + '/' + measure['pressure_and_pulse'][1]      + '. ' +
            'Пульс: ' + measure['pressure_and_pulse'][2] + ' удара в минуту'                              + '. ' +
            'Температура: ' + measure['temperature'] + '°C'                                               + '. ' ;
}

function construct_card_date(measure) {
    let date = new Date(measure['date']);
    return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + ' ' + date.getHours() + ':'
        + date.getMinutes();
}

function update_card(card, new_measure) {
    card.getElementsByClassName('card-text')[0].innerText = construct_card_measure_text(new_measure);
    card.getElementsByClassName('current-photo')[0].innerText = new_measure['current_photo'];
}

function reminder_status(success_btn, disturbance) {
    const id = get_reminder_card_id(success_btn);
    measure_status(id, disturbance);
    success_btn.parentElement.parentElement.remove();
    remove_reminder(id);
}

function remove_reminder(id) {
    makeRequest('/remove_reminder/' + id, 'get', null, function (err, resp, body) {
        console.log('Ауе', err, resp, body);
        // TODO May be add a notification here
    })
}

function view_info(reminder_card) {
    const modal = $('#reminderModal');
    modal.modal('show');
    console.log(modal.find('img')[0], reminder_card.getElementsByClassName('current-photo')[0].innerText);

    let current_photo = reminder_card.getElementsByClassName('current-photo')[0].innerText;
    modal.find('img').attr('src', fix_photo_url(current_photo));

    modal.find('h5').text(reminder_card.getElementsByClassName('card-title')[0].innerText);
    modal.find('p').text(reminder_card.getElementsByClassName('card-text')[0].innerText);
    modal.find('.card-subtitle').text(reminder_card.getElementsByClassName('card-subtitle')[0].innerText);
}

function fix_photo_url(photo_url) {
    const config = remote.getGlobal('sharedObject');
    if (!photo_url.startsWith('http://'))
        photo_url = 'http://' + config.serverHost + ':' + config.serverPort + '/media/' + photo_url;
    return photo_url;
}