request = require('request');
remote = require('electron').remote;
pug = require('pug');
path = require('path');

let TOKEN = null;

document.addEventListener('DOMContentLoaded', function () {

    transition();

    $('#loginForm').on('submit', function(event) {
		event.preventDefault();
		var form = {
			'username': $('#loginInput').val(),
			'password': $('#passwordInput').val()
		};
		makeRequest('/api-token-auth/', 'post', form, function(err, response, body) {
			if (response.statusCode !== 200) {
				if (!$('.alert').length){
					var alertTmpl = pug.compileFile(path.join(__dirname, 'sections/alert.pug'))();
					$('#content').prepend(alertTmpl);
				}
				return;
			}
			const config = remote.getGlobal('sharedObject');
			config['token'] = JSON.parse(body)['token'];
			TOKEN = JSON.parse(body)['token'];
			console.log(config.token);
            showIndex();
		});
	});
});

function showIndex() {
    var login_content = $('#login-content');
    login_content.css('opacity', '0');

    login_content.bind('transitionend', function () {
        login_content.remove();

        var compiledPug = pug.compileFile(path.join(__dirname, 'sections/sidebar.pug'));
        var main_content = $('#main-content');
        main_content.append(compiledPug());
        var compiledPug = pug.compileFile(path.join(__dirname, 'sections/new_reminders.pug'));
        main_content.append(compiledPug());

        main_content.css('opacity', '1');
    });
}

function transition() {
    var welcome = $('#welcome');
    console.log(welcome.css('visibility'));
    console.log(welcome.css('opacity'));
    welcome.css('visibility', 'visible');
    welcome.css('opacity', '1');

    welcome.on('transitionend', function () {
        welcome.css('opacity', '0');
        console.log('LoLLOL');
        welcome.on('transitionend', function () {
            welcome.css('visibility', 'hidden');

            var loginForm = $('#loginForm');
            loginForm.css('visibility', 'visible');
            loginForm.css('opacity', '1');
        });
    });
}

function go_to_view(view_name) {
    closeView();
    let main_block = $('#main-block');
    main_block.hide({duration: 400, done: function () {
        main_block.remove();
        main_block = pug.compileFile(path.join(__dirname, 'sections/', view_name))();
        const main_content = $('#main-content');
        main_content.append(main_block);
    }});
}

function makeRequest(url, method, form, fun) {
    var config = remote.getGlobal('sharedObject');
    var options = {
        method: method,
        url: 'http://' + config.serverHost + ':' + config.serverPort + url,
        form: form,
        headers: {
            'Authorization': (TOKEN === null) ? '' : 'Token ' + TOKEN
        }
    };
    request(options, fun);
}

function get_reminder_card_id(success_btn) {
    return success_btn.parentElement.getElementsByClassName('measure_id')[0].innerText;
}

function measure_status(measure_id, disturbance) {
    makeRequest('/api/measures/' + measure_id, 'get', null, function (err, response, body) {
        console.log('ARR', err, response, body);
        const user_id = JSON.parse(body)['user'];
        const form = {
            disturbance: disturbance,
            viewed: true
        };
        console.log('form', form);
        makeRequest('/api/measures/' + measure_id + '/', 'PATCH', form, function (err, response, body) {
            console.log(err, response);
        })
    });
}
