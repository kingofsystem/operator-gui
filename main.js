const electron = require('electron');
const url = require('url');
const path = require('path');
const locals = {/* ...*/};
const pug = require('electron-pug')({pretty: true}, locals);
const {app, BrowserWindow} = electron;
const Config = require('electron-config');

const AppSettings = new Config();
var mainWindow;

// Listen for app to be ready
app.on('ready', function(){
	configureElectron();
    mainWindow = new BrowserWindow({});
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.pug'),
        protocol: 'file:',
        slashes: true
    }));

});

function configureElectron() {
	global.sharedObject = {
		serverHost: 'diagnost.i-i.space',
		serverPort: '80'
	};

	if (AppSettings.get('show_notifications') === undefined)
        AppSettings.set('show_notifications', true);
}