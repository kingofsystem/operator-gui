# Operator GUI

App for the  doctor who make a decision about a employee`s health status. 

It`s a part of automated diagnostician. 

## Screenshots

###### Login screen

![Login screen](assets/images/login_scr.png)

###### Main screen

![Main screen](assets/images/main_scr.png)

## Start info

Prepare folder with `npm install`, then use `npm start .`.

Information about deploying can be found [here](https://electronjs.org/docs/tutorial/application-distribution)