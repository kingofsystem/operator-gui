request = require('request');
remote = require('electron').remote;
pug = require('pug')
path = require('path');

document.addEventListener('DOMContentLoaded', function () {

    transition();

	$('#loginForm').on('submit', function(event) {
		event.preventDefault();
		var config = remote.getGlobal('sharedObject');
		var req = {
			username: $('#loginInput').val(),
			password: $('#passwordInput').val()
		}
		request.post({url: 'http://' + config.serverHost + ':' + config.serverPort + '/api-token-auth/', form: req}, function(err, response, body) {
			if (response.statusCode != 200) {
				if (!$('.alert').length){
					var alertTmpl = pug.compileFile(path.join(__dirname, 'sections/alert.pug'))();
					$('#content').prepend(alertTmpl);
				}
				return;
			}

			config.token = JSON.parse(body)['token'];
			document.href = path.join(__dirname, 'index.pug');
		});
	});
});

