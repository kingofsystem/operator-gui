request = require('request');
remote = require('electron').remote;
pug = require('pug');
path = require('path');

if (typeof AppSettings === 'undefined'){
    const Config = require('electron-config');

    const AppSettings = new Config();
}

let settings_view_loaded = true;

function init_settings_view() {
    $('#notif-checkbox').prop('checked', AppSettings.get('show_notifications'));
}

function notif_checkbox_click(checkbox) {
    AppSettings.set('show_notifications', $('#notif-checkbox').prop('checked'))
}