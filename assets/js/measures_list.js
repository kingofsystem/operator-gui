request = require('request');
remote = require('electron').remote;
pug = require('pug');
path = require('path');

let measures_list_loaded = true;

load_measures();

function load_measures() {
    makeRequest('/api/measures/', 'get', null, function(err, response, body) {
        const list = JSON.parse(body);
        list.forEach(function(measure) {
            measure['pressure_and_pulse'] = JSON.parse(measure['pressure_and_pulse']);
            if (measure['user'] !== parseInt(current_user_info['id']))
                return;
            let compiledPug = pug.compileFile(path.join(__dirname, 'sections/reminder_card.pug'));

            let html = compiledPug({
                card_title: current_user_info['card_title'],
                measure_text: construct_card_measure_text(measure),
                measure_date: construct_card_date(measure),
                measure_id: measure['id'],
                photo_url: measure['current_photo'],
                status_good_click: 'btn_measure_click(this, false)',
                status_bad_click: 'btn_measure_click(this, true)'
            });
            console.log(html);
            $('#main-block-content').prepend(html);
        });
    });
}

function btn_measure_click(success_btn, disturbance) {
    const measure_id = get_reminder_card_id(success_btn);
    measure_status(measure_id, disturbance);
}