request = require('request');
remote = require('electron').remote;
pug = require('pug');
path = require('path');

let users_view_loaded = true;
current_user_info = {};

function obtain_users() {
    makeRequest('/prepared_users/', 'get', null, function (err, response, body) {
        console.log('Users', body);
        let users = JSON.parse(body);
        users.forEach(function (user) {
            add_user(user);
        });
    });
}

obtain_users();

function add_user(user_dict) {
    const config = remote.getGlobal('sharedObject');
    const compiledPug = pug.compileFile(path.join(__dirname, 'sections/user_card.pug'));
    const html = compiledPug({
        card_photo: 'http://' + config.serverHost + ':' + config.serverPort + user_dict['photo'],
        card_title: user_dict['name'] + ' ' + user_dict['surname'] + ' ' + user_dict['lastname'],
        card_text: construct_user_card_text(user_dict),
        user_id: user_dict['id']
    });
    $('#main-block-content').append(html);
}

function construct_user_card_text(user_dict) {
    return 'Дата рождения: ' + user_dict['birthday'] + '. ' +
            'Нарушения: ' + user_dict['disturbances'] + '. ' +
            'Организация: ' + user_dict['organization'] + '. ' +
            'Вес: ' + user_dict['weight'] + '. ';
}

function view_user_info(user_card) {
    current_user_info = {
        id: user_card.getElementsByClassName('user-id')[0].innerText,
        card_title: user_card.getElementsByClassName('card-title')[0].innerText,
        card_text: user_card.getElementsByClassName('card-title')[0].innerText
    };
    go_to_view('measures_list.pug');
}
